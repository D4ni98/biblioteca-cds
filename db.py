import mysql.connector        

class DataBase:
    def __init__(self,user, password, host, database):
        self.__user = user
        self.__password = password
        self.__host = host
        self.__database = database

    def inicioDataBase(self):
        self.__cnx = mysql.connector.connect(user = self.__user, password = self.__password, host=self.__host, database= self.__database)
        
    def getConexion(self):
        return self.__cnx

    def closeDataBase(self):
        self.__cnx.close()

