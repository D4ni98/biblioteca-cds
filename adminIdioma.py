from Idioma import Idioma


class AdminIdioma:
    def __init__(self, database):
        self.__db = database

    def insert (self, idioma):
        self.__db.inicioDataBase()
        cursor = self.__db.getConexion().cursor()
        insert= "INSERT INTO idiomas (nombre) VALUES ('%s')" %(idioma.getNombre())
        cursor.execute(insert)
        self.__db.getConexion().commit()
        cursor.close()
        self.__db.closeDataBase()

    def getAll (self):
        listaNueva=[]
        self.__db.inicioDataBase()
        cursor = self.__db.getConexion().cursor()
        select = "SELECT * FROM idiomas" 
        cursor.execute(select)
        lista = cursor.fetchall()
        for i in lista:
            nuevo = Idioma(i[1])
            nuevo.setId(i[0])
            listaNueva.append(nuevo)

        cursor.close()
        self.__db.closeDataBase()
        
        return listaNueva

    def getByNombre(self,nombre):
        self.__db.inicioDataBase()
        cursor = self.__db.getConexion().cursor()
        select = "SELECT * FROM idiomas i WHERE i.nombre = '%s'" % nombre
        cursor.execute(select)
        lista = cursor.fetchall()
        cursor.close()
        self.__db.closeDataBase()
        return lista

    def getId(self, idioma):
        self.__db.inicioDataBase()
        cursor = self.__db.getConexion().cursor()
        select = "SELECT id FROM idiomas i WHERE i.nombre LIKE '%s'" % idioma
        cursor.execute(select) 
        lista = cursor.fetchall()
        cursor.close()
        self.__db.closeDataBase()
        return lista[0][0]


    def update(self, nuevoDato, id):
        self.__db.inicioDataBase()
        cursor = self.__db.getConexion().cursor()
        update = "UPDATE idiomas SET nombre='%s' WHERE id='%i'" % (nuevoDato, id)
        cursor.execute(update)
        self.__db.getConexion().commit()
        cursor.close()
        self.__db.closeDataBase()
    
    def delete (self, id):
        self.__db.inicioDataBase()
        cursor = self.__db.getConexion().cursor()
        delete = "DELETE FROM idiomas WHERE id='%i'" % (id)
        cursor.execute(delete)
        self.__db.getConexion().commit()
        cursor.close()
        self.__db.closeDataBase()
        








        




